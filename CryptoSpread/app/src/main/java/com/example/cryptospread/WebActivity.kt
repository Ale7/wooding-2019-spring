package com.example.cryptospread

import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem

import kotlinx.android.synthetic.main.activity_web.*
import kotlinx.android.synthetic.main.content_web.*

class WebActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        val url = intent.getStringExtra("cmcUrl")
        webview1.loadUrl(url)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }

}
