package com.example.cryptospread

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query

@Dao
interface PairDataDao {
    @Query("SELECT * from pairData")
    fun getAll(): List<PairData>

    @Query("SELECT id from pairData")
    fun getId(): Array<Int>

    @Query("SELECT spread from pairData")
    fun getSpread(): Array<String>

    @Query("SELECT pair from pairData")
    fun getPair(): Array<String>

    @Query("SELECT description from pairData")
    fun getDescription(): Array<String>

    @Query("SELECT cmcUrl from pairData")
    fun getCmcUrl(): Array<String>

    @Query("SELECT lowPrice from pairData")
    fun getLowPrice(): Array<String>

    @Query("SELECT highPrice from pairData")
    fun getHighPrice(): Array<String>

    @Query("SELECT lowVolume from pairData")
    fun getLowVolume(): Array<String>

    @Query("SELECT highVolume from pairData")
    fun getHighVolume(): Array<String>

    @Query("SELECT lowName from pairData")
    fun getLowName(): Array<String>

    @Query("SELECT highName from pairData")
    fun getHighName(): Array<String>

    @Query("SELECT lowInfo from pairData")
    fun getLowInfo(): Array<String>

    @Query("SELECT highInfo from pairData")
    fun getHighInfo(): Array<String>

    @Query("SELECT lowUrl from pairData")
    fun getLowUrl(): Array<String>

    @Query("SELECT highUrl from pairData")
    fun getHighUrl(): Array<String>

    @Insert(onConflict = REPLACE)
    fun insert(pairData: PairData)

    @Query("DELETE from pairData")
    fun deleteAll()
}