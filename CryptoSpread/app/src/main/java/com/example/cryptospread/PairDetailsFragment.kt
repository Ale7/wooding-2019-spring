package com.example.cryptospread

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cryptospread.databinding.FragmentPairDetailsBinding
import kotlinx.android.synthetic.main.fragment_pair_details.*
import android.content.Intent
import android.graphics.Color
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*

class PairDetailsFragment : Fragment() {

    private lateinit var historicalSpreadChart: LineChart

    companion object {

        private const val PAIRMODEL = "model"

        fun newInstance(pairModel: PairModel): PairDetailsFragment {
            val args = Bundle()
            args.putSerializable(PAIRMODEL, pairModel)
            val fragment = PairDetailsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentPairDetailsBinding =
            FragmentPairDetailsBinding.inflate(inflater, container, false)

        val model = arguments!!.getSerializable(PAIRMODEL) as PairModel
        fragmentPairDetailsBinding.pairModel = model

        return fragmentPairDetailsBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val model = arguments!!.getSerializable(PAIRMODEL) as PairModel

        setHistoricalSpreadChart()

        button_buy_fee.setOnClickListener {
            val intent = Intent(context, WebActivity::class.java)
            intent.putExtra("cmcUrl", model.lowInfo)
            startActivity(intent)
        }

        button_buy.setOnClickListener {
            val intent = Intent(context, WebActivity::class.java)
            intent.putExtra("cmcUrl", model.lowUrl)
            startActivity(intent)
        }

        button_sell_fee.setOnClickListener {
            val intent = Intent(context, WebActivity::class.java)
            intent.putExtra("cmcUrl", model.highInfo)
            startActivity(intent)
        }

        button_sell.setOnClickListener {
            val intent = Intent(context, WebActivity::class.java)
            intent.putExtra("cmcUrl", model.highUrl)
            startActivity(intent)
        }

    }

    private fun setHistoricalSpreadChart() {
        historicalSpreadChart = historical_spread_chart

        val description = Description()
        description.text = "Historical spread %"
        description.textColor = Color.WHITE
        description.textSize = 12f
        historicalSpreadChart.description = description
        historicalSpreadChart.legend.isEnabled = false
        historicalSpreadChart.setPinchZoom(false)

        val xAxis = historicalSpreadChart.xAxis
        xAxis.setDrawGridLines(false)
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.isEnabled = true
        xAxis.setDrawAxisLine(false)

        val yLeft = historicalSpreadChart.axisLeft
        yLeft.isEnabled = false

        xAxis.labelCount = 12

        xAxis.isGranularityEnabled = true
        xAxis.granularity = 1f

        val yRight = historicalSpreadChart.axisRight
        yRight.setDrawAxisLine(true)
        yRight.setDrawGridLines(false)
        yRight.isEnabled = false

        setGraphData()

        historicalSpreadChart.animateY(1000)
    }

    private fun setGraphData() {

        val entries = ArrayList<Entry>()
        entries.add(BarEntry(0f, 1.22f))
        entries.add(BarEntry(1f, 0.87f))
        entries.add(BarEntry(2f, 0.27f))
        entries.add(BarEntry(3f, 0.04f))
        entries.add(BarEntry(4f, 0.13f))
        entries.add(BarEntry(5f, 0.76f))
        entries.add(BarEntry(6f, 1.45f))
        entries.add(BarEntry(7f, 1.33f))
        entries.add(BarEntry(8f, 1.36f))
        entries.add(BarEntry(9f, 0.45f))
        entries.add(BarEntry(10f, 0.23f))
        entries.add(BarEntry(11f, 0.08f))

        val lineDataSet = LineDataSet(entries, "Line Data Set")
        lineDataSet.setCircleColor(Color.WHITE)
        lineDataSet.color = Color.WHITE

        val data = LineData(lineDataSet)
        data.setValueTextColor(Color.WHITE)
        data.setValueTextSize(12f)

        historicalSpreadChart.data = data
        historicalSpreadChart.xAxis.textColor = Color.WHITE
        historicalSpreadChart.invalidate()
    }

}
