package com.example.cryptospread

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "pairData")
data class PairData(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "spread") var spread: String,
    @ColumnInfo(name = "pair") var pair: String,
    @ColumnInfo(name = "description") var description: String,
    @ColumnInfo(name = "cmcUrl") var cmcUrl: String,
    @ColumnInfo(name = "lowPrice") var lowPrice: String,
    @ColumnInfo(name = "highPrice") var highPrice: String,
    @ColumnInfo(name = "lowVolume") var lowVolume: String,
    @ColumnInfo(name = "highVolume") var highVolume: String,
    @ColumnInfo(name = "lowName") var lowExchangeName: String,
    @ColumnInfo(name = "highName") var highExchangeName: String,
    @ColumnInfo(name = "lowInfo") var lowExchangeInfo: String,
    @ColumnInfo(name = "highInfo") var highExchangeInfo: String,
    @ColumnInfo(name = "lowUrl") var lowExchangeUrl: String,
    @ColumnInfo(name = "highUrl") var highExchangeUrl: String
)
