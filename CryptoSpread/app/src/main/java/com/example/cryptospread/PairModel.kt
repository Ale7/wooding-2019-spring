package com.example.cryptospread

import java.io.Serializable

data class PairModel(
    val id: Int,
    val spread: String,
    val pair: String,
    val description: String,
    val cmcUrl: String,
    val lowPrice: String,
    val highPrice: String,
    val lowVolume: String,
    val highVolume: String,
    val lowName: String,
    val highName: String,
    val lowInfo: String,
    val highInfo: String,
    val lowUrl: String,
    val highUrl: String = ""
) : Serializable
