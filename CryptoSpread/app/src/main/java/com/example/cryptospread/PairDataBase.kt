package com.example.cryptospread

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(
    entities = [PairData::class],
    version = 1,
    exportSchema = false
)
abstract class PairDataBase : RoomDatabase() {
    abstract fun pairDataDao(): PairDataDao
}