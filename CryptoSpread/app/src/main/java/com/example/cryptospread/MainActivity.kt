package com.example.cryptospread

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem

class MainActivity : AppCompatActivity(), PairListFragment.OnPairSelected {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.root_layout, PairListFragment.newInstance(), "pairList")
                .commit()
        }
    }

    override fun onPairSelected(pairModel: PairModel) {
        val detailsFragment =
            PairDetailsFragment.newInstance(pairModel)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.root_layout, detailsFragment, "pairDetails")
            .addToBackStack(null)
            .commit()

        supportActionBar!!.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

}
