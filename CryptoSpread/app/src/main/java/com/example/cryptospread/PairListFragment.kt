package com.example.cryptospread

import android.arch.persistence.room.Room
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cryptospread.databinding.RecyclerItemPairModelBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PairListFragment : Fragment() {
    private lateinit var ids: Array<Int>
    private lateinit var spreads: Array<String>
    private lateinit var pairs: Array<String>
    private lateinit var descriptions: Array<String>
    private lateinit var cmcUrls: Array<String>
    private lateinit var lowPrices: Array<String>
    private lateinit var highPrices: Array<String>
    private lateinit var lowVolumes: Array<String>
    private lateinit var highVolumes: Array<String>
    private lateinit var lowNames: Array<String>
    private lateinit var highNames: Array<String>
    private lateinit var lowInfos: Array<String>
    private lateinit var highInfos: Array<String>
    private lateinit var lowUrls: Array<String>
    private lateinit var highUrls: Array<String>
    private lateinit var listener: OnPairSelected

    companion object {

        fun newInstance(): PairListFragment {
            return PairListFragment()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is OnPairSelected) {
            listener = context
        } else {
            throw ClassCastException(context.toString() + " must implement OnPairSelected.")
        }

        val db = Room.databaseBuilder(
            context,
            PairDataBase::class.java, "pairs.db"
        ).build()

        GlobalScope.launch {
            ids = db.pairDataDao().getId()
            spreads = db.pairDataDao().getSpread()
            pairs = db.pairDataDao().getPair()
            descriptions = db.pairDataDao().getDescription()
            cmcUrls = db.pairDataDao().getCmcUrl()
            lowPrices = db.pairDataDao().getLowPrice()
            highPrices = db.pairDataDao().getHighPrice()
            lowVolumes = db.pairDataDao().getLowVolume()
            highVolumes = db.pairDataDao().getHighVolume()
            lowNames = db.pairDataDao().getLowName()
            highNames = db.pairDataDao().getHighName()
            lowInfos = db.pairDataDao().getLowInfo()
            highInfos = db.pairDataDao().getHighInfo()
            lowUrls = db.pairDataDao().getLowUrl()
            highUrls = db.pairDataDao().getHighUrl()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(
            R.layout.fragment_pair_list, container,
            false
        )
        val activity = activity as Context
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.layoutManager = GridLayoutManager(activity, 1)
        recyclerView.adapter = PairListAdapter(activity)
        return view
    }

    internal inner class PairListAdapter(context: Context) : RecyclerView.Adapter<ViewHolder>() {

        private val layoutInflater = LayoutInflater.from(context)

        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
            val recyclerPairModelBinding =
                RecyclerItemPairModelBinding.inflate(layoutInflater, viewGroup, false)
            return ViewHolder(recyclerPairModelBinding.root, recyclerPairModelBinding)
        }

        override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
            val pair = PairModel(
                ids[position], spreads[position], pairs[position],
                descriptions[position], cmcUrls[position],
                lowPrices[position], highPrices[position],
                lowVolumes[position], highVolumes[position],
                lowNames[position], highNames[position],
                lowInfos[position], highInfos[position],
                lowUrls[position], highUrls[position]
            )
            viewHolder.setData(pair)
            viewHolder.itemView.setOnClickListener { listener.onPairSelected(pair) }
        }

        override fun getItemCount() = pairs.size
    }

    internal inner class ViewHolder constructor(
        itemView: View,
        private val recyclerItemPairListBinding:
        RecyclerItemPairModelBinding
    ) :
        RecyclerView.ViewHolder(itemView) {

        fun setData(pairModel: PairModel) {
            recyclerItemPairListBinding.pairModel = pairModel
        }
    }

    interface OnPairSelected {
        fun onPairSelected(pairModel: PairModel)
    }

}
