package com.example.homework1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.lang.NumberFormatException

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textViewConcat: TextView = findViewById(R.id.concat_textView)
        val textViewAdd: TextView = findViewById(R.id.add_textView)

        val editText1: EditText = findViewById(R.id.first_editText)
        val editText2: EditText = findViewById(R.id.second_editText)

        val buttonConcat: Button = findViewById(R.id.concat_button)
        val buttonAdd: Button = findViewById(R.id.add_button)

        buttonConcat.setOnClickListener {
            textViewConcat.text = editText1.text
            textViewConcat.append(editText2.text)
        }

        buttonAdd.setOnClickListener {
            var editText1Double = 0.0
            var editText2Double = 0.0
            try {
                editText1Double = editText1.text.toString().toDouble()
                editText2Double = editText2.text.toString().toDouble()
            } catch (e: NumberFormatException) { }
            textViewAdd.text = (editText1Double + editText2Double).toString()
        }
    }
}
